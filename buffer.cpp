#include "stdafx.h"
#include "buffer.h"

buffer::buffer(FILE *f, int s)
{
    cur_bit_num = buff = 0;
    fp = f;
    correct_size(s);
}

void buffer::correct_size(int s)
{
    if (s == 128)
        bits_for_cs = 7;
    if (s == 256)
        bits_for_cs = 8;
    if (s == 512)
        bits_for_cs = 9;
}

void buffer::out_byte(void) 
{
    if (cur_bit_num == 8) {
        cur_bit_num = 0;
        fwrite(&buff, sizeof(uchar), 1, fp);
        buff = 0;
    }
}

void buffer::in_byte(void) 
{
    if (cur_bit_num == 0) {
        cur_bit_num = 8;
        fread(&buff, sizeof(uchar), 1, fp);
    }
}

void buffer::print_bit(int x) 
{
    out_byte();
    buff = (buff << 1) + (x & 1);
    ++cur_bit_num;
}

void buffer::print_pixel(uchar x)
{
    uint gt = 1 << (8 - 1);
    for (int i = 0; i < 8; i++) {
        out_byte();
        buff <<= 1;
        buff += (x & gt) >> (8 - 1);
        x &= ~gt;
        x <<= 1;
        ++cur_bit_num;
    }
}

void buffer::print_block(block b) 
{
    // print y
    uint gt = 1 << (bits_for_cs - 1);
    for (int i = 0; i < bits_for_cs; i++) {
        out_byte();
        buff <<= 1;
        buff += (b.y & gt) >> (bits_for_cs - 1);
        b.y &= ~gt;
        b.y <<= 1;
        ++cur_bit_num;
    }
    // print x
    for (int i = 0; i < bits_for_cs; i++) {
        out_byte();
        buff <<= 1;
        buff += (b.x & gt) >> (bits_for_cs - 1);
        b.x &= ~gt;
        b.x <<= 1;
        ++cur_bit_num;
    }
    // print alpha
    out_byte();
    if (b.alpha < 0) {
        buff = (buff << 1) + 1;
        ++cur_bit_num;
    } else {
        buff <<= 1;
        ++cur_bit_num;
    }
    b.alpha = abs(b.alpha);
    gt = 1 << (bits_for_alpha - 1);
    for (int i = 0; i < bits_for_alpha; i++) {
        out_byte();
        buff <<= 1;
        buff += int(b.alpha * 2.0);
        b.alpha *= 2;
        if (int(b.alpha) == 1)
            b.alpha -= 1;
        ++cur_bit_num;
    }
    // print betta
    out_byte();
    if (b.betta < 0) {
        buff = (buff << 1) + 1;
        ++cur_bit_num;
    } else {
        buff <<= 1;
        ++cur_bit_num;
    }
    int betta = abs(b.betta);
    gt = 1 << (bits_for_betta - 1);
    for (int i = 0; i < bits_for_betta; i++) {
        out_byte();
        buff <<= 1;
        buff += (betta & gt) >> (bits_for_betta - 1);
        betta &= ~gt;
        betta <<= 1;
        ++cur_bit_num;
    }
    // print transformation
    gt = 1 << (bits_for_tr - 1);
    for (int i = 0; i < bits_for_tr; i++) {
        out_byte();
        buff <<= 1;
        buff += (b.transformation & gt) >> (bits_for_tr - 1);
        b.transformation &= ~gt;
        b.transformation <<= 1;
        ++cur_bit_num;
    }
}

int buffer::read_bit(void) {
    in_byte();
    int gt = 1 << 7;
    int x = (buff & gt) >> 7;
    buff &= ~gt;
    buff <<= 1;
    --cur_bit_num;
    return x;
}

uchar buffer::read_pixel(void)
{
    uchar x = 0;
    int gt = 1 << 7;
    for (int i = 0; i < 8; i++) {
        in_byte();
        x += (buff & gt) >> 7;
        if (i != 8 - 1)
            x <<= 1;
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    return x;
}

block buffer::read_block()
{
    block b;
    // reading every peace of block :)
    // read y
    b.y = 0;
    int gt = 1 << 7;
    for (int i = 0; i < bits_for_cs; i++) {
        in_byte();
        b.y += (buff & gt) >> 7;
        if (i != bits_for_cs - 1)
            b.y <<= 1;
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    // read x
    b.x = 0;
    for (int i = 0; i < bits_for_cs; i++) {
        in_byte();
        b.x += (buff & gt) >> 7;
        if (i != bits_for_cs - 1)
            b.x <<= 1;
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    // read alpha
    in_byte();
    double ng = ((buff & gt) >> 7) ? -1.0 : 1.0;
    buff &= ~gt;
    buff <<= 1;
    --cur_bit_num;
    b.alpha = 0;
    for (int i = 0; i < bits_for_alpha; i++) {
        in_byte();
        b.alpha += (1.0 * ((buff & gt) >> 7)) / (1.0 * (1 << (i + 1)));
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    b.alpha *= ng;
    // print betta
    in_byte();
    int ngb = ((buff & gt) >> 7) ? -1.0 : 1.0;
    buff &= ~gt;
    buff <<= 1;
    --cur_bit_num;
    b.betta = 0;
    for (int i = 0; i < bits_for_betta; i++) {
        in_byte();
        b.betta += (buff & gt) >> 7;
        if (i != bits_for_betta - 1)
            b.betta <<= 1;
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    b.betta *= ngb;
    // print transformation
    b.transformation = 0;
    for (int i = 0; i < bits_for_tr; i++) {
        in_byte();
        b.transformation += (buff & gt) >> 7;
        if (i != bits_for_tr - 1)
            b.transformation <<= 1;
        buff &= ~gt;
        buff <<= 1;
        --cur_bit_num;
    }
    return b;
}

void buffer::final_print() 
{
    buff <<= 8 - cur_bit_num;
    fwrite(&buff, sizeof(uint), 1, fp);
}

buffer::~buffer(void)
{
}
