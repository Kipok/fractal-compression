#ifndef TABLE_H
#define TABLE_H

#include <map>
#include <vector>
using namespace std;

#define max_symbols 257
#define EOF_symbol 256
#define mp make_pair
#define max_freq 1073741824

class FenwickTree 
{
public:
    FenwickTree(void)
    {
        memset(a, 0, sizeof(a));
        memset(b, 0, sizeof(b));
    }
    void add(int i, unsigned int v) 
    {
        a[i] += v;
        while(b[i] += v, i |= i + 1, i < max_symbols); 
    }
    long long get(int i) 
    {
       long long r = 0; 
       while(i >= 0) { 
           r += b[i];
           i = (i & (i + 1)) - 1;
       }
       return r; 
    }
    void update(void) 
    {
        for (int i = 0; i < max_symbols; ++i) {
            a[i] >>= 1;
            ++a[i];
        }
        memset(b, 0, sizeof(b));
        for (int i = 0; i < max_symbols; ++i) {
            long long k = a[i];
            a[i] = 0;
            add(i, k);
        }
    }
    long long a[max_symbols];
    long long b[max_symbols];
};

class table
{
public:
    table(int m);
    ~table(void);
    long long get(int i, unsigned int s0, unsigned int s1, unsigned int aggr);
    void update(int i, unsigned int s0, unsigned int s1, unsigned int aggr);
    int mode;
private:
    vector <vector <FenwickTree> > tables;
    FenwickTree freq;
};

#endif // TABLE_H