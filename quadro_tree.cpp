#include "stdafx.h"
#include "quadro_tree.h"

void quadro_tree::initialize(block ***b, int block_size, int w, int h) 
{
    (*b) = new block *[h / block_size];
    for (int i = 0; i * block_size < h; i++) {
        (*b)[i] = new block[w / block_size];
        for (int j = 0; j * block_size < w; j++)
            (*b)[i][j].block_size = block_size;
    }
}

quadro_tree::quadro_tree(int q, int w1, int h1)
{
    quality = q;
    threshold = 7000 / (quality * quality);
    w = w1;
    h = h1;
    b = new block**[6];
    max_size = 6;
    for (int i = 0; i < max_size; i++)
        initialize(&b[i], 1 << (i + 1), w, h);
}

void quadro_tree::del_blocks(block ***b, int block_size, int h) 
{
    for (int i = 0; i * block_size < h; i++)
        delete [] (*b)[i];
    delete [] (*b);
}

quadro_tree::~quadro_tree(void)
{
    for (int i = 0; i < max_size; i++)
        del_blocks(&b[i], 1 << (i + 1), h);
    delete b;
}
