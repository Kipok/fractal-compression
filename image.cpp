#include "stdafx.h"
#include "image.h"

image::image(int w1, int h1)
{
    w = w1;
    h = h1;
    block_size = -1;
    data = new uchar[w*h];
}

image::image()
{
    block_size = -1;
}

void image::initialize(image &I) {
    w = I.w;
    h = I.h;
    block_size = I.block_size;
    data = new uchar[w*h];
    for (int y = 0; y < I.h; y++)
        for (int x = 0; x < I.w; x++)
            (*this)[y][x] = I[y][x];
}

void image::initialize(int w1, int h1, int bs) {
    w = w1;
    h = h1;
    block_size = bs;
    data = new uchar[w*h];
}

 uchar* image::operator[](int x) 
{
    return data + w * x;
}

image::~image(void)
{
    if (block_size != -1) {
        for (int x = 0; x <= w - block_size; x++) {
            delete[] sum[x];
            delete[] square_sum[x];
        }
        delete[] sum;
        delete[] square_sum;
    }
    delete data;
}