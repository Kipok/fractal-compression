#pragma once
#include "block.h"
#include <cmath>

typedef unsigned long uint; 
typedef unsigned char uchar;
#define round(x) int((x) + 0.5)
#define bits_for_alpha 8
#define bits_for_betta 8
#define bits_for_tr 3

class buffer
{
public:
    buffer(FILE *f, int s);
    ~buffer(void);
    void print_block(block x);
    void print_pixel(uchar x);
    void print_bit(int x);
    void final_print();
    block read_block(void);
    uchar read_pixel(void);
    int read_bit(void);
    void correct_size(int s);
    uint cur_bit_num;
private:
    uchar buff;
    uint bits_for_cs;
    void out_byte(void);
    void in_byte(void);
    FILE *fp;
};

