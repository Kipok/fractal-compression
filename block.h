#pragma once

#define inf 1073741824

enum {
    no_rotate = 0,
    rotate_90 = 1,
    rotate_180 = 2,
    rotate_270 = 3,
    flip = 4
};

class block
{
public:
    block(int bs);
    block();
    ~block(void);
    double alpha;
    int betta;
    int block_size;
    int x, y;
    int ssd;
    int transformation;
};

