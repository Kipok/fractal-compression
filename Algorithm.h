void Compress(unsigned long *image,const char *compressed_file_name,int w, int h,int block_size,int quality);

unsigned long* Decompress(const char *compressed_file_name,int &w,int &h);