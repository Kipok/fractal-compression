// FractalCompression.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Algorithm.h"
#include "BitmapEx.h"

void SaveImage(wchar_t *fn,unsigned long *data,int w,int h)
{
	CBitmapEx bmp;

	bmp.Create(w,h);
	for(int y=0;y<h;y++)
	{
		for(int x=0;x<w;x++)
		{
			bmp.SetPixel(x,y,data[y*w+x]);
		}
	}

	bmp.Save(fn);

}

unsigned long* LoadImage(wchar_t *fn,int &w,int &h)
{
	CBitmapEx bmp;
	bmp.Load(fn);
	w = bmp.GetWidth();
	h = bmp.GetHeight();

	unsigned long* data = new unsigned long[w*h];

	for(int y=0;y<h;y++)
	{
		for(int x=0;x<w;x++)
		{
			data[y*w+x] = bmp.GetPixel(x,y);
		}
	}

	return data;
}

int _tmain(int argc, _TCHAR* argv[])
{
	try{
		int mode = 0;
		int quality = 0;
		int block_size = 8;
		wchar_t *image_fn=0;
		unsigned char* archive=0;

		for(int i=1;i<argc;i++)
		{
			if(wcscmp(argv[i],L"-c")==0)
			{
				i++;
				if(argv[i])
					image_fn = argv[i];
				else
					throw "Not enough input arguments";

				i++;
				if(argv[i])
				{
					 int sz = wcslen(argv[i]);
					 archive = new unsigned char[sz*2];
					 wcstombs_s(0,(char*)archive,sz*2,argv[i],sz*2);
				}
				else
					throw "Not enough input arguments";
				mode = 1;

			}
			else if(wcscmp(argv[i],L"-d")==0)
			{
				i++;
				if(argv[i])
				{
					 int sz = wcslen(argv[i]);
					 archive = new unsigned char[sz*2];
					 wcstombs_s(0,(char*)archive,sz*2,argv[i],sz*2);
				}
				else
					throw "Not enough input arguments";

				i++;
				if(argv[i])
					image_fn = argv[i];
				else
					throw "Not enough input arguments";
				mode = 2;

			}
			else if(wcscmp(argv[i],L"-s")==0)
			{
				i++;
				if(argv[i])
				{
					block_size = _wtoi(argv[i]);
					if(block_size<1)
						throw "Block size should be positive number";
				}
				else
					throw "Not enough input arguments";
			}

			else if(wcscmp(argv[i],L"-q")==0)
			{
				i++;
				if(argv[i])
				{
					quality = _wtoi(argv[i]);
					if((quality<1)||(quality>100))
						throw "Quality should be in range [1,100]";
				}
				else
					throw "Not enough input arguments";
			}
			else
			{
				throw "Unknown command line option";
			}
		}

		if(mode==0)
		{
			printf("Work mode wasn't speciefied.\nDoing nothing...\nUsage:\n -c input.bmp output.fr [-s block_size] [-q qualitty]\n -d input.fr output.bmp\n");
			system("pause");
		}
		else if(mode==1)
		{
			int w=0,h=0;
			printf("Compression mode\nReading input image\n");
			unsigned long *imgd = LoadImage(image_fn,w,h);
			if( (w%256) || (w!=h) )
				throw "Input image should be 256x256 or 512x512 pixels";

			printf("Running compression proccess\n");
			Compress(imgd,(const char*)archive,w,h,block_size,quality);
			printf("Done\n");
			delete[] imgd;
		}
		else if(mode==2)
		{
			int w=0,h=0;
			printf("Decompression mode\nRunning decompression proccess\n");
			unsigned long* imgd = Decompress((const char*)archive,w,h);
			if(imgd==0)
				throw "No image data was returned";
			printf("Writing output image\n");
			SaveImage(image_fn,imgd,w,h);
		}

		delete[] archive;
	}
	catch(char *e)
	{
		printf("Error:");
		printf("%s\n",e);
		system("pause");
	}
	return 0;

}

