#pragma once

#include "block.h"

class quadro_tree
{
public:
    quadro_tree(int q, int w1, int h1);
    ~quadro_tree(void);
    block ***b;
    int quality;
    int threshold;
    int w, h;
    int max_size;
private:
    void initialize(block ***b, int block_size, int w, int h);
    void del_blocks(block ***b, int block_size, int h);
};

