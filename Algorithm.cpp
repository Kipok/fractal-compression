#include "stdafx.h"
#include "image.h"
#include "block.h"
#include "buffer.h"
#include "quadro_tree.h"

#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

#define int_to_r(k) uchar(((k) & 0x00FF0000) >> 16)
#define int_to_g(k) uchar(((k) & 0x0000FF00) >> 8)
#define int_to_b(k) uchar(((k) & 0x000000FF))
#define rgb_to_int(r, g, b) (((r) << 16) + ((g) << 8) + (b))
#define to_uchar(x) uchar(max(min(255.0, (x)), 0.0))
#define to_dchar(x) double(max(min(255.0, (x)), 0.0))
#define y_it_num 1000
#define uv_it_num 2000
#define qt_y_num 350
#define qt_uv_num 700
#define sqr(a) ((a) * (a))
#define dec_num 100
#define enough_num 10
#define search_num 1

const int dx[8] = {0, 1, -1, 0, 1, 1, -1, -1};
const int dy[8] = {-1, 0, 0, 1, 1, -1, 1, -1};

int size(int x) {
    int t = (x == 0);
    while (x) {
        ++t;
        x /= 10;
    }
    return t;
}

void print_progress(int civ) {
    static int lst = 0;
    for (int i = 0; i < lst; i++)
        putc('\b', stdout);
    for (int i = 0; i < lst; i++)
        putc(' ', stdout);
    for (int i = 0; i < lst; i++)
        putc('\b', stdout);
    printf("%d", civ);
    lst = size(civ);
}

pair <int, int> transf_pos(int y0, int x0, int s, int transformation) 
{
    s -= 2;
    if (transformation == no_rotate) {
        return make_pair(y0, x0);
    }
    if (transformation == rotate_90) {
        return make_pair(y0 + s, x0);
    }
    if (transformation == rotate_180) {
        return make_pair(y0 + s, x0 + s);
    }
    if (transformation == rotate_270) {
        return make_pair(y0, x0 + s);
    }
    if (transformation == (no_rotate | flip)) {
        return make_pair(y0, x0 + s);
    }
    if (transformation == (rotate_90 | flip)) {
        return make_pair(y0 + s, x0 + s);
    }
    if (transformation == (rotate_180 | flip)) {
        return make_pair(y0 + s, x0);
    }
    if (transformation == (rotate_270 | flip)) {
        return make_pair(y0, x0);
    }
}

inline pair <int, int> transf_displmnt(int dy, int dx, int transformation)
{
    if (transformation == no_rotate) {
        return make_pair(dy, dx);
    }
    if (transformation == rotate_90) {
        return make_pair(-dx, dy);
    }
    if (transformation == rotate_180) {
        return make_pair(-dy, -dx);
    }
    if (transformation == rotate_270) {
        return make_pair(dx, -dy);
    }
    if (transformation == (no_rotate | flip)) {
        return make_pair(dy, -dx);
    }
    if (transformation == (rotate_90 | flip)) {
        return make_pair(-dx, -dy);
    }
    if (transformation == (rotate_180 | flip)) {
        return make_pair(-dy, dx);
    }
    if (transformation == (rotate_270 | flip)) {
        return make_pair(dx, dy);
    }
}

void improve_ssd(image &I, image **c, block &b, int y2, int x2, int y1, int x1, int &civ, int tr) 
{
    // (y1 x1) - source block; (y2 x2) - found block
    double alpha = 0.0;
    int betta = 0, sign = 0;
    int sum_a = I.sum[x1][y1 + b.block_size - 1] - ((y1 == 0) ? 0 : I.sum[x1][y1 - 1]);
    int sum_a_square = I.square_sum[x1][y1 + b.block_size - 1] - ((y1 == 0) ? 0 : I.square_sum[x1][y1 - 1]);
    int sum_b = c[y2 & 1][x2 & 1].sum[x2 >> 1][(y2 >> 1) + b.block_size - 1] - (((y2 >> 1) == 0) ? 0 : c[y2 & 1][x2 & 1].sum[x2 >> 1][(y2 >> 1) - 1]);
    int sum_b_square = c[y2 & 1][x2 & 1].square_sum[x2 >> 1][(y2 >> 1) + b.block_size - 1] - (((y2 >> 1) == 0) ? 0 : c[y2 & 1][x2 & 1].square_sum[x2 >> 1][(y2 >> 1) - 1]);
    for (int i = 0; i < 8; i++) {
        if (tr != 8)
            i = tr;
        int dot_pr = 0, ssd_cur = 0;
        // counting dot products
        pair <int, int> cr = transf_pos(y2, x2, 2 * b.block_size, i);
        int y = cr.first;
        int x = cr.second;
        for (int dy = 0; dy < b.block_size; dy++) {
            for (int dx = 0; dx < b.block_size; dx++) {
                pair <int, int> cdr = transf_displmnt(dy, dx, i);
                int ydy = cdr.first;
                int xdx = cdr.second;
                int ic = I[y1 + dy][x1 + dx];
                int cc = c[y & 1][x & 1][(y >> 1) + ydy][(x >> 1) + xdx];
                dot_pr += I[y1 + dy][x1 + dx] * c[y & 1][x & 1][(y >> 1) + ydy][(x >> 1) + xdx];
            }
        }
        if (sqr(b.block_size) * sum_b_square - sqr(sum_b) != 0)
            alpha = 1.0 * (sqr(b.block_size) * dot_pr - sum_a * sum_b) / (sqr(b.block_size) * sum_b_square - sqr(sum_b));
        else
            alpha = 0;
        if (alpha > 0.97)
            alpha = 0.97;
        if (alpha < -0.97)
            alpha = -0.97;
        if (1.0 * sqr(b.block_size) != 0)
            betta = 1.0 * (sum_a - alpha * sum_b) / (1.0 * (sqr(b.block_size)));
        else
            betta = 0;
        if (betta > 255)
            betta = 255;
        if (betta < -255)
            betta = -255;
        ssd_cur = sum_a_square - dot_pr * 2 * alpha - sum_a * 2 * betta + sum_b_square * sqr(alpha) + sum_b * 2 * alpha * betta + sqr(betta) * sqr(b.block_size);
        if (ssd_cur < b.ssd) {
            b.ssd = ssd_cur;
            b.alpha = alpha;
            b.betta = betta;
            b.transformation = i;
            sign = 1;
        }
        if (tr != 8)
            break;
    }
    if (sign) {
        b.x = x2;
        b.y = y2;
    }
    civ += sign;
}

void random_search(image &I, image **c, block &b, int y, int x, int &civ) 
{
    //  Here I assume that image has equal width and height as it states in the task. Let's use I.h as an image size
    int sign = 0;
    for (int i = 0; i < search_num; i++) {
        if (sign)
            break;
        int vx = b.x, vy = b.y;
        int h = I.h;
        for (int rsize = h; rsize >= 1; rsize >>= 1) {
            int rbx = min(h - (b.block_size << 1), vx + rsize);
            int lux = max(0, vx - rsize);
            int rby = min(h - (b.block_size << 1), vy + rsize);
            int luy = max(0, vy - rsize);
            int xr = lux + rand() % (rbx - lux);
            int yr = luy + rand() % (rby - luy);
            int cc = civ;
            improve_ssd(I, c, b, yr, xr, y, x, civ, 8);
            if (civ > cc) {
                sign = 1;
                break;
            }
        }
    }
}

void patch_match(image &I, image **c, block **b, int block_size, int it_num) 
{
    // initialising all blocks by random values
    int x = 0, y = 0, count_impr_v = 0;
    for (y = 0; y * block_size < I.h; y++)
        for (x = 0; x * block_size < I.w; x++) {
            b[y][x].x = rand() % (I.w - 2 * block_size);
            b[y][x].y = rand() % (I.h - 2 * block_size);
            improve_ssd(I, c, b[y][x], b[y][x].y, b[y][x].x, y * block_size, x * block_size, count_impr_v, 8);
        }
    int num_of_null = 0, i = 0;
    x = y = 0;
   // printf("Number of vectors improved on current iteration:   ");
    for (; i < it_num; i++) {
        if (!count_impr_v)
            ++num_of_null;
        if (num_of_null > enough_num)
            break;
        count_impr_v = 0;
        for (y = (i & 1) ? I.h / block_size - 1 : 0; y * block_size < I.h && y >= 0; y += (i & 1) ? -1 : 1) {
            for (x = (i & 1) ? I.w / block_size - 1 : 0; x * block_size < I.w && x >= 0; x += (i & 1) ? -1 : 1) {
                // trying to improve blocks ssd with vectors of their neighbores
                for (int r = 0; r < 8; r++) {
                    int xv = x + dx[r];
                    int yv = y + dy[r];
                    if (xv >= 0 && yv >= 0 && xv * block_size < I.w && yv * block_size < I.h)
                        if (b[yv][xv].x - dx[r] >= 0 && b[yv][xv].y - dy[r] >= 0 && (b[yv][xv].x - dx[r]) * block_size < I.w - 1 && (b[yv][xv].y - dy[r]) * block_size < I.h - 1)
                            improve_ssd(I, c, b[y][x], b[yv][xv].y - dy[r], b[yv][xv].x - dx[r], y * block_size, x * block_size, count_impr_v, b[yv][xv].transformation);
                }
                for (int r = 0; r < 8; r++) {
                    int xv = b[y][x].x + dx[r];
                    int yv = b[y][x].y + dy[r];
                    if (xv >= 0 && yv >= 0 && xv * block_size < I.w && yv * block_size < I.h)
                        if (b[yv][xv].x - dx[r] >= 0 && b[yv][xv].y - dy[r] >= 0 && (b[yv][xv].x - dx[r]) * block_size < I.w - 1 && (b[yv][xv].y - dy[r]) * block_size < I.h - 1)
                            improve_ssd(I, c, b[y][x], b[yv][xv].y - dy[r], b[yv][xv].x - dx[r], y * block_size, x * block_size, count_impr_v, b[yv][xv].transformation);
                }
                random_search(I, c, b[y][x], y * block_size, x * block_size, count_impr_v);
            }
        }
       /* if (i % 5 == 0)
            print_progress(count_impr_v);*/
    }
    printf("\n%d iterations were completed\n", i);
}

inline uchar averaging(image &I, int y, int x) 
{
    return (I[y][x] + I[y][x + 1] + I[y + 1][x] + I[y + 1][x + 1]) >> 2;
}

void make_small_image(image &I, image &res, int block_size) 
{
    // scaling image in order to match source blocks with blocks twice their size
    res.initialize(I.w / 2, I.h / 2, block_size);
    for (int y = 0; (y << 1) < I.h - 1; y++)
        for (int x = 0; (x << 1) < I.w - 1; x++)
            res[y][x] = averaging(I, y << 1, x << 1);
    // precounting partition sums and square sums for counting SSD later
    res.sum = new int *[res.w];
    res.square_sum = new int *[res.w];
    for (int x = 0; x <= res.w - block_size; x++) {
        res.sum[x] = new int[res.h];
        res.square_sum[x] = new int[res.h];
        memset(res.sum[x], 0, res.h * sizeof(int));
        memset(res.square_sum[x], 0, res.h * sizeof(int));
        for (int y = 0; y < res.h; y++) {
            if (y) {
                res.sum[x][y] += res.sum[x][y - 1];
                res.square_sum[x][y] += res.square_sum[x][y - 1];
            }
            for (int t = x; t < x + block_size; t++) {
                res.sum[x][y] += res[y][t];
                res.square_sum[x][y] += res[y][t] * res[y][t];
            }
        }
    }
}

void match_blocks(image &I, block **b, int block_size, int it_num) 
{
    image **c, Ic;
    c = new image *[2];
    c[0] = new image[2];
    c[1] = new image[2];
    // copy I to Ic
    Ic.initialize(I);

    make_small_image(Ic, c[0][0], block_size);
    for (int y = 0; y < I.h; y++)
        for (int x = 0; x < I.w; x++)
            Ic[y][x] = I[y][x + 1 == I.w ? x : x + 1];
    make_small_image(Ic, c[0][1], block_size);
    for (int y = 0; y < I.h; y++)
        for (int x = 0; x < I.w; x++)
            Ic[y][x] = I[y + 1 == I.h ? y : y + 1][x];
    make_small_image(Ic, c[1][0], block_size);
    for (int y = 0; y < I.h; y++)
        for (int x = 0; x < I.w; x++)
            Ic[y][x] = I[y + 1 == I.h ? y : y + 1][x + 1 == I.w ? x : x + 1];
    make_small_image(Ic, c[1][1], block_size);

    // precounting partition sums and square sums for the main image
    I.sum = new int *[I.w];
    I.square_sum = new int *[I.w];
    for (int x = 0; x <= I.w - block_size; x++) {
        I.sum[x] = new int[I.h];
        I.square_sum[x] = new int[I.h];
        memset(I.sum[x], 0, I.h * sizeof(int));
        memset(I.square_sum[x], 0, I.h * sizeof(int));
        for (int y = 0; y < I.h; y++) {
            if (y) {
                I.sum[x][y] += I.sum[x][y - 1];
                I.square_sum[x][y] += I.square_sum[x][y - 1];
            }
            for (int t = x; t < x + block_size; t++) {
                I.sum[x][y] += I[y][t];
                I.square_sum[x][y] += I[y][t] * I[y][t];
            }
        }
    }
    patch_match(I, c, b, block_size, it_num);
    delete [] c[0];
    delete [] c[1];
    delete [] c;
}

int log2(int x) {
    int t = 0;
    while (x) {
        x >>= 1;
        ++t;
    }
    return t - 1;
}

void print_tree(image &I, quadro_tree &t, buffer &b, int block_size, int y0, int x0, int s)
{
   // printf("%d %d %d %d\n", block_size, y0, x0, s);
    for (int y = y0; y * block_size < y0 * block_size + s; y++) {
        for (int x = x0; x * block_size < x0 * block_size + s; x++) {
            if (block_size > 2 && t.b[log2(block_size) - 1][y][x].ssd > t.threshold * sqr(block_size)) {
                b.print_bit(1);
           //     fprintf(f, "1\n");
                print_tree(I, t, b, block_size >> 1, y * 2, x * 2, block_size);
            } else {
                b.print_bit(0);
                if (block_size > 2)
                    b.print_block(t.b[log2(block_size) - 1][y][x]);
                else {
                    b.print_pixel(I[y * block_size][x * block_size]);
                    b.print_pixel(I[y * block_size][x * block_size + 1]);
                    b.print_pixel(I[y * block_size + 1][x * block_size]);
                    b.print_pixel(I[y * block_size + 1][x * block_size + 1]);
                }
             //   fprintf(f, "0\n%d %d %lf %d %d\n", t.b[log2(block_size) - 1][y][x].y, t.b[log2(block_size) - 1][y][x].x, t.b[log2(block_size) - 1][y][x].alpha, t.b[log2(block_size) - 1][y][x].betta, t.b[log2(block_size) - 1][y][x].transformation);
            }
        }
    }
}

void print_canal(block **b, buffer &buff, int block_size, int s) 
{
    buff.correct_size(s);
 //   FILE *f = fopen("comp.fr", "wt");
    for (int y = 0; y * block_size < s; y++)
        for (int x = 0; x * block_size < s; x++) {
            buff.print_block(b[y][x]);
     //       fprintf(f, "%d %d %lf %d %d\n", b[y][x].y, b[y][x].x, b[y][x].alpha, b[y][x].betta, b[y][x].transformation);
        }
   // fclose(f);
}

void initialize(block ***b, int block_size, int s) 
{
    (*b) = new block *[s / block_size];
    for (int y = 0; y * block_size < s; y++) {
        (*b)[y] = new block[s / block_size];
        for (int x = 0; x * block_size < s; x++)
            (*b)[y][x].block_size = block_size;
    }
}

void del_blocks(block ***b, int block_size, int s) 
{
    for (int i = 0; i * block_size < s; i++)
        delete [] (*b)[i];
    delete [] *b;
}

void make_arithmetic(string s, const char *cfn)
{
    // using arithmetic compression for coefficients
    printf("Please, wait a few moments more for I need to compress files:\n");
    string s1 = "arithmetic.exe c ";
    s1 += s + ' ';
    s1 += cfn;
    s1 += " ppm";
    system(s1.c_str());
    remove(s.c_str());
    printf("\%\n");
}

void Compress(uint *img, const char *compressed_file_name, int w, int h, int block_size, int quality)
{
    // start counting time
    srand(time(NULL));
    int tm = time(NULL);
    // convert to YUV
	image Y(w, h), U(w / 2, h / 2), V(w / 2, h / 2);
    for (int y = 0; y < h; y++)
        for (int x = 0; x < w; x++) {
            int k = img[y * w + x];
            Y[y][x] = to_uchar(0.299 * int_to_r(k) + 0.587 * int_to_g(k) + 0.114 * int_to_b(k));
            if (!(y & 1) && !(x & 1)) {
                U[y >> 1][x >> 1] = to_uchar(-0.168736 * int_to_r(k) - 0.331264 * int_to_g(k) + 0.5 * int_to_b(k) + 128.0);
                V[y >> 1][x >> 1] = to_uchar(0.5 * int_to_r(k) - 0.418688 * int_to_g(k) - 0.081312 * int_to_b(k) + 128.0);
            }
        }
    // I use different file to write coordinates in order to rewrite everything in compressed_file_name with arithmetic compression
    string s = compressed_file_name;
    s += ".cmp";
    FILE *fp = fopen(s.c_str(), "wb");
    buffer buff(fp, h);

    if (quality == 0) {
        // it means we don't need to use quadrotree
        block **by, **bu, **bv;
        initialize(&by, block_size, Y.h);
        initialize(&bu, block_size, U.h);
        initialize(&bv, block_size, V.h);


#pragma omp parallel sections
     {
    #pragma omp section
        match_blocks(Y, by, block_size, y_it_num);
    #pragma omp section
        match_blocks(U, bu, block_size, uv_it_num);
    #pragma omp section
        match_blocks(V, bv, block_size, uv_it_num);
     }        
        // it's the marker that we don't use quadrotree
        w |= 1 << 10;
        fwrite(&w, sizeof(int), 1, fp);
        fwrite(&h, sizeof(int), 1, fp);
        fwrite(&block_size, sizeof(int), 1, fp);

        print_canal(by, buff, block_size, Y.h);
        print_canal(bu, buff, block_size, U.h);
        print_canal(bv, buff, block_size, V.h);
     
        buff.final_print();

        del_blocks(&by, block_size, Y.h);
        del_blocks(&bu, block_size, U.h);
        del_blocks(&bv, block_size, V.h);
    } else {
        fwrite(&w, sizeof(int), 1, fp);
        fwrite(&h, sizeof(int), 1, fp);
        fwrite(&block_size, sizeof(int), 1, fp);

        quadro_tree ty(quality, w, h), tu(quality, w / 2, h / 2), tv(quality, w / 2, h / 2);
        printf("This is for Y:\n");

    image IfY[6], IfU[6], IfV[6];

        for (int i = 0; i < ty.max_size; i++)
            IfY[i].initialize(Y);
    #pragma omp parallel for
        for (int i = 1; i < ty.max_size; i++)
            match_blocks(IfY[i], ty.b[i], 1 << (i + 1), qt_y_num);
        print_tree(Y, ty, buff, (1 << ty.max_size), 0, 0, Y.h);
        
        buff.correct_size(U.h); 
        printf("This is for U:\n");
        if ((1 << tu.max_size) == U.h / 2)
            --tu.max_size;
        for (int i = 0; i < ty.max_size; i++)
            IfU[i].initialize(U);
    #pragma omp parallel for
        for (int i = 1; i < tu.max_size; i++)
            match_blocks(IfU[i], tu.b[i], 1 << (i + 1), qt_uv_num);
        print_tree(U, tu, buff, (1 << tu.max_size), 0, 0, U.h);

        buff.correct_size(V.h);
        if ((1 << tv.max_size) == V.h / 2)
            --tv.max_size;
        printf("This is for V:\n"); 
        for (int i = 0; i < ty.max_size; i++)
            IfV[i].initialize(V);
    #pragma omp parallel for
        for (int i = 1; i < tv.max_size; i++)
            match_blocks(IfV[i], tv.b[i], 1 << (i + 1), qt_uv_num);
        print_tree(V, tv, buff, (1 << tv.max_size), 0, 0, V.h);
        buff.final_print();
    }
   	fclose(fp);
    make_arithmetic(s, compressed_file_name);
    remove(s.c_str());
    int ftm = time(NULL) - tm;
    printf("\nTime elapsed: %ds or approximetely %dm\n", ftm, round(1.0 * ftm / 60.0));
}

void decompress_canal(double *I, buffer &buff, int s, int block_size)
{
    buff.correct_size(s);
    block **b;
    for (int y = 0; y < s; y++)
        for (int x = 0; x < s; x++)
            I[y * s + x] = 128.0;

   // FILE *f = fopen("decomp.fr", "wt");

    b = new block *[s / block_size];
    for (int y = 0; y * block_size < s; y++) {
        b[y] = new block[s / block_size];
        for (int x = 0; x * block_size < s; x++) {
            b[y][x] = buff.read_block();
         //   fprintf(f, "%d %d %lf %d %d\n", b[y][x].y, b[y][x].x, b[y][x].alpha, b[y][x].betta, b[y][x].transformation);
        }
    }
  //  fclose(f);
    // this is actually main cycle for decompressing --> it uses transformations found in the match_blocks function
    for (int r = 0; r < dec_num; r++) {
        int yb = 0, xb = 0;
        for (int y = 0, yb = 0; y < s; y += block_size, ++yb)
            for (int q = 0; q < block_size; q++)
                for (int x = 0, xb = 0; x < s; x += block_size, ++xb)
                    for (int t = 0; t < block_size; t++) {
                        pair <int, int> cr = transf_pos(b[yb][xb].y, b[yb][xb].x, 2 * block_size, b[yb][xb].transformation);
                        pair <int, int> cdr = transf_displmnt(q, t, b[yb][xb].transformation);
                        int yc = cr.first + 2 * cdr.first;
                        int xc = cr.second + 2 * cdr.second;
                        I[(y + q) * s + x + t] = (((I[yc * s + xc] + I[yc * s + xc + 1] + I[(yc + 1) * s + xc] + I[(yc + 1) * s + xc + 1]) / 4.0) * b[yb][xb].alpha + b[yb][xb].betta);
                    }
    }
    del_blocks(&b, block_size, s);
}

void fill_UV(double *U, double *V, double *UC, double *VC, int s)
{
    // filling U and V images with absent pixels.
    for (int y = 0; y < s / 2; y++)
        for (int x = 0; x < s / 2; x++) {
            U[y * 2 * s + x * 2] = UC[y * s / 2 + x];
            V[y * 2 * s + x * 2] = VC[y * s / 2 + x];
        }
    for (int y = 0; y < s; y++)
        for (int x = 1; x < s; x += 2) {
            if (x == s - 1) {
                V[y * s + x] = V[y * s + x - 1];
                U[y * s + x] = U[y * s + x - 1];
            }
            else {
                V[y * s + x] = (V[y * s + x - 1] + V[y * s + x + 1]) / 2;
                U[y * s + x] = (U[y * s + x - 1] + U[y * s + x + 1]) / 2;
            }
        }
   for (int y = 1; y < s; y += 2)
        for (int x = 0; x < s; x++) {
            if (y == s - 1) {
                V[y * s + x] = V[(y - 1) * s + x];
                U[y * s + x] = U[(y - 1) * s + x];
            }
            else {
                V[y * s + x] = (V[(y - 1) * s + x] + V[(y + 1) * s + x]) / 2;
                U[y * s + x] = (U[(y - 1) * s + x] + U[(y + 1) * s + x]) / 2;
            }
        }
}

void decompress_tree(double *I, buffer &buff, int block_size, int y0, int x0, int s, int h)
{
    for (int i = y0; i * block_size < y0 * block_size + s; i++) {
        for (int j = x0; j * block_size < x0 * block_size + s; j++) {
            if (buff.read_bit()) {
              //  fprintf(f, "1\n");
                decompress_tree(I, buff, block_size >> 1, i * 2, j * 2, block_size, h);
            }
            else {
                if (block_size > 2) {
                    block b = buff.read_block();
                  //  fprintf(f, "0\n%d %d %lf %d %d\n", b.y, b.x, b.alpha, b.betta, b.transformation);
                    for (int q = 0; q < block_size; q++)
                        for (int t = 0; t < block_size; t++) {
                            pair <int, int> cr = transf_pos(b.y, b.x, 2 * block_size, b.transformation);
                            pair <int, int> cdr = transf_displmnt(q, t, b.transformation);
                            int yc = cr.first + 2 * cdr.first;
                            int xc = cr.second + 2 * cdr.second;
                            int y = i * block_size;
                            int x = j * block_size;
                            I[(y + q) * h + x + t] = (((I[yc * h + xc] + I[yc * h + xc + 1] + I[(yc + 1) * h + xc] + I[(yc + 1) * h + xc + 1]) / 4.0) * b.alpha + b.betta);
                        }
                } else {
                    int y = i * block_size;
                    int x = j * block_size;
                    I[y * h + x] = buff.read_pixel();
                    I[y * h + x + 1] = buff.read_pixel();
                    I[(y + 1) * h + x] = buff.read_pixel();
                    I[(y + 1) * h + x + 1] = buff.read_pixel();
                }
            }
        }
    }
}

uint* Decompress(const char *compressed_file_name, int &w, int &h)
{
	// read data
    int block_size;
    string s = compressed_file_name;
    s += ".cmp";
    string s1 = "arithmetic.exe d ";
    s1 += compressed_file_name;
    s1 += ' ' + s;
    system(s1.c_str());
    FILE *fp;
    if (!(fp = fopen(s.c_str(), "rb"))) {
        printf("Can't open compressed image. ERROR!");
        exit(0);
    }
    fread(&w, sizeof(int), 1, fp);
    fread(&h, sizeof(int), 1, fp);
    fread(&block_size, sizeof(int), 1, fp);
    
    uint *img = new uint[h * h];
    double *Y = new double[h * h];
    double *UC = new double[h * h / 4];
    double *VC = new double[h * h / 4];
    double *U = new double[h * h];
    double *V = new double[h * h];
    buffer buff(fp, h);
    
    if (w & (1 << 10)) {
        w &= ~(1 << 10);
        decompress_canal(Y, buff, h, block_size);
        decompress_canal(UC, buff, h / 2, block_size);
        decompress_canal(VC, buff, h / 2, block_size);
    } else {
        for (int y = 0; y < h; y++)
            for (int x = 0; x < h; x++)
               Y[y * h + x] = 128.0;
        for (int y = 0; y < h / 2; y++)
            for (int x = 0; x < h / 2; x++)
               UC[y * h / 2 + x] = 128.0;        
        for (int y = 0; y < h / 2; y++)
            for (int x = 0; x < h / 2; x++)
               VC[y * h / 2 + x] = 128.0;

        int uv_block_size = 64;
        if (h / 4 == uv_block_size)
            uv_block_size = 32;

        for (int i = 0; i < dec_num; i++) {
            buff.correct_size(h);
            decompress_tree(Y, buff, 64, 0, 0, h, h);
            buff.correct_size(h / 2);
            decompress_tree(UC, buff, uv_block_size, 0, 0, h / 2, h / 2);
            buff.correct_size(h / 2);
            decompress_tree(VC, buff, uv_block_size, 0, 0, h / 2, h / 2);
            // I'm starting to read file again in order not to memorize blocks
            fseek(fp, 3 * sizeof(int), SEEK_SET);
            buff.cur_bit_num = 0;
        }
    }
    fill_UV(U, V, UC, VC, h);
    // output data
	for(int y = 0; y < h; y++) {
		for(int x = 0; x < h; x++) {
            uchar yc, uc, vc, r, g, b;
            yc = round(to_dchar(Y[y * h + x]));
            uc = round(to_dchar(U[y * h + x]));
            vc = round(to_dchar(V[y * h + x]));
            r = to_uchar(yc + 1.4075 * (vc - 128.0));
            g = to_uchar(yc - 0.3455 * (uc - 128.0) - 0.7169 * (vc - 128.0));
            b = to_uchar(yc + 1.7790 * (uc - 128.0));
            img[y * h + x] = rgb_to_int(r, g, b);
		}
    }
    delete [] Y;
    delete [] VC;
    delete [] UC;
    delete [] U;
    delete [] V;
    fclose(fp);
    remove(s.c_str());
	return img;
}