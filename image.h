#pragma once

typedef unsigned char uchar;

class image
{
public:
    image();
    image(int w1, int h1);
    ~image(void);
    int w, h;
    int block_size;
    int **sum;
    int **square_sum;
    uchar * operator[](int x);
    void initialize(image &I);
    void initialize(int w1, int h1, int bs);
private:
    uchar *data;
};

